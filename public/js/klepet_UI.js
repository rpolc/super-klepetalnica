/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

function dodajSlike(vhodnoBesedilo){
  var povezava = vhodnoBesedilo.split(' ');
  for(var i=0; i<povezava.length; i++){
    if(povezava[i].match(/(https?:\/\/.*\.(?:png|jpg|gif))/gi)){
      vhodnoBesedilo += "<br>"+"<img src='"+povezava[i]+"' style='margin-left:20px;', width='200px' />";
    }
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  if(sporocilo.slice(-7)=="&#9756;"){
    return divElementHtmlTekst(sporocilo);
  }
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  var jeSlika = sporocilo.match(/(https?:\/\/.*\.(?:png|jpg|gif))/i);
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else if (jeSlika){
    return $('<div style="font-weight: bold"></div>').html(sporocilo);
  }
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  sporocilo = dodajSlike(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

var nadimki=[""];
function dodajNadimek(vzdevek, nadimek){
  var dodaj=1;
  for(var i=0; i<nadimki.length; i++){
    if(nadimki[i].includes(vzdevek)){
      nadimki[i]=nadimek+"("+vzdevek+")";
      dodaj=0;
      break;
    }
  }
  if(dodaj){
    if(nadimek[0]="") {nadimki[0]=nadimek+"("+vzdevek+")";}
    else {nadimki.push(nadimek+"("+vzdevek+")");}
  }
}

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    nadimki=[""];
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var vnos = sporocilo.besedilo;
    
    if(vnos.includes("Trenutni uporabniki na kanalu ")){
      var izpis=vnos;
    }
    
    else if(vnos.includes("se je preimenoval v")){
      var izpis=vnos;
      var deli=vnos.split(" ");
      console.log(deli[0]);
      for(var i=0; i<nadimki.length; i++){
        if(nadimki[i].includes(deli[0])){
          izpis=nadimki[i]+" se je preimenoval v "+deli[5];
          break;
        }
      }
      for(var i=0; i<nadimki.length; i++){
        if(nadimki[i].includes(deli[0])){
          var nick=nadimki[i].split("(");
          var name=deli[5].split(".");
          nadimki[i]=nick[0]+"("+name[0]+")";
          break;
        }
      }
    }
    
    else if(vnos.includes(" (zasebno): ")){
      var deli=vnos.split(":");
      var delcki=deli[0].split(" ");
      for(var i=0; i<nadimki.length; i++){
        if(nadimki[i].includes(delcki[0])){
          delcki[0]=nadimki[i];
          break;
        }
      }
      var izpis=delcki[0]+" (zasebno):"+deli[1];
    }
    else if(vnos.includes(": ")){
      var deli=vnos.split(":");
      for(var i=0; i<nadimki.length; i++){
        if(nadimki[i].includes(deli[0])){
          deli[0]=nadimki[i];
          break;
        }
      }
      var izpis=deli[0]+": "+deli[1];
    }
    var novElement=divElementEnostavniTekst(izpis);
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      var preimenovan=0;
      for(var j=0; j<nadimki.length; j++){
        if(nadimki[j].includes(uporabniki[i])){
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadimki[j]));
          preimenovan=1;
          break;
        }
      }
      if(!preimenovan){$('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));}
    }
    
    //krcni uporabnika s klikom nanj v seznamu
    $('#seznam-uporabnikov div').click(function() {
      klepetApp.procesirajUkaz('/zasebno ' + $(this).text());
      $('#poslji-sporocilo').val('/zasebno "'+$(this).text()+'" "&#9756;" ');
      procesirajVnosUporabnika(klepetApp, socket);
    });
  });
  
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
